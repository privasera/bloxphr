'use strict';

/**
 * @ngdoc function
 * @name phrPrototypeApp.controller:ProfileCtrl
 * @description
 * # ProfileCtrl
 * Controller of the phrPrototypeApp
 */
angular.module('phrPrototypeApp')
  .controller('ProfileCtrl', function ($scope, $location, $http, $anchorScroll, account, $localStorage) {

  	$scope.profile = {};
    //$scope.email = 'Casen@test.com';

    /* Get the user's information */
       if($localStorage.userFetched==undefined)
       {

           $http({
               method: 'GET',
               url: '/user',
           }).then(function(response) {

                   var result = response.data.Result;
                   if(result == 'Success') {
                       $scope.name = response.data.Name;
                       $scope.email = response.data.Email;
                       $scope.dob = response.data.DOB;
                       $scope.imageUrl = response.data.Image;

                       $localStorage.userFetched=response.data;


                       /* Make a request for test results just to exploit */
                       $http({
                           method: 'GET',
                           url: '/getTestResults?email=' + $scope.email,
                       }).then(function(response) {
                           },
                           function(response) {
                           });
                   }
               },
               function(response) {
               });

       }
        else{

           $scope.name =  $localStorage.userFetched.Name;
           $scope.email =  $localStorage.userFetched.Email;
           $scope.dob =  $localStorage.userFetched.DOB;
           $scope.imageUrl =  $localStorage.userFetched.Image;
           /* Make a request for test results just to exploit */
           $http({
               method: 'GET',
               url: '/getTestResults?email=' + $scope.email,
           }).then(function(response) {
               },
               function(response) {
               });

       }


    $scope.navClick = function (element) {
        var old = $location.hash();
        $location.hash(element);
        $anchorScroll();
        //reset to old to keep any additional routing logic from kicking in
        $location.hash(old);
    };

  	account.account(function(err, accountInfo) {

  		
  		$scope.profile = accountInfo;

  		//Shims for HL7 weirdness.
  		var tmpDOB = moment(accountInfo.dob[0].date).format('YYYY-MM-DD');

  		$scope.profile.dob = tmpDOB;
  		$scope.profile.primaryEmail = accountInfo.email[0].email;
  	});

  	//console.log($scope.profile);

  });
