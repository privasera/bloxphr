'use strict';

/**
 * @ngdoc function
 * @name phrPrototypeApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the phrPrototypeApp
 */
angular.module('phrPrototypeApp')
  .controller('LoginCtrl', function ($scope, $location, authentication,$localStorage) {

        $scope.$storage = $localStorage;
        $scope.LoginStatus = "Logged Out";


    $scope.login = function () {
        $scope.LoginStatus = "Logging In";
  		authentication.login($scope.inputLogin, $scope.inputPassword, function(err, email) {
  			if (err) {
  				$scope.error = err;
  			} else {
                $scope.LoginStatus = "Logged In";
                $localStorage.AuthenticatedUser = $scope.inputLogin;
                delete $localStorage.userFetched
  				$location.path('/home');
  			}
 		});
  	};
  });
