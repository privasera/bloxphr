'use strict';

/**
 * @ngdoc function
 * @name phrPrototypeApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the phrPrototypeApp
 */
angular.module('phrPrototypeApp')
  .controller('MainCtrl', function ($scope, $location, authentication,$localStorage) {

        $scope.$storage = $localStorage;
        $scope.LoginStatus = "Logged Out";

  	$scope.login = function (isValid) {
        $scope.LoginStatus = "Logging In";
  		authentication.login($scope.inputLogin, $scope.inputPassword, function(err) {
  			if (err) {
  				$scope.error = err;
  			} else {
                $scope.LoginStatus = "Logged In";
                $localStorage.AuthenticatedUser = $scope.inputLogin;
                delete $localStorage.userFetched
  				$location.path('/home');
  			}
 		});
  	};

  });
