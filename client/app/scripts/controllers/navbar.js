'use strict';

/**
 * @ngdoc function
 * @name phrPrototypeApp.controller:NavbarCtrl
 * @description
 * # NavbarCtrl
 * Controller of the phrPrototypeApp
 */
angular.module('phrPrototypeApp')
    .controller('NavbarCtrl', function ($rootScope, $scope, $location, authentication,$localStorage) {

        $scope.loginStatus = false;

        $scope.logout = function()
        {
             delete $localStorage.AuthenticatedUser;
            delete $localStorage.userFetched;
            $location.url('/');
            location.reload();
        }

        function checkAuthStatus() {
            authentication.authStatus(function (err, res) {
                if (err) {
                    $scope.loginStatus = false;
                    console.log(err);
                } else {
                    if (!res) {
                        $scope.loginStatus = false;
                       // delete $localStorage.AuthenticatedUser;
                    } else {
                        $scope.loginStatus = true;
                    }
                }
            });
        }

        checkAuthStatus();

        $rootScope.$on('$locationChangeStart', function (event, newUrl, oldUrl) {
            checkAuthStatus();
        });

    });