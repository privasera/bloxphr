'use strict';

/**
 * @ngdoc service
 * @name phrPrototypeApp.authentication
 * @description
 * # authentication
 * Service in the phrPrototypeApp.
 */
angular.module('phrPrototypeApp')
    .service('authentication', function authentication($location, $http,$localStorage) {


        var login = false;


        //TODO:  Hygiene here for max length of inputs.
        this.login = function (username, password, callback) {
            if (username && password) {
                /* Set the session for the security vulnerability demo */
                $http({
                    method: 'POST',
                    url: '/login',
                    data: {
                        email: username,
                        password: password
                    }
                }).then(function(response) {
                    var result = response.data.Result;
                    if( result == 'Success') {
                        callback(null);
                    }
                    else {
                        callback('Invalid Login Credentials')   
                    }
                }, 
                function(response) {
                    callback('Connection Error. Check Your Internet.')   
                });
            }
        };

        this.logout = function (callback) {
            
        	var err = null;

        	//Stubbed logout.
        	if (err) {
        		callback(err);
        	} else {
        		callback(null);
        	}
        };

        //This would be a server call, but now just stubbed with $location.
        this.authStatus = function (callback) {

            if ($location.path() === "/" || $location.path() === "/login" || $location.path() === "/register" || $location.path() === "/reset") {
                callback(null, false);
            } else {
                callback(null, true);
            }

        };

    });
