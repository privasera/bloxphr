
/*
 |--------------------------------------------------------------------------
 | Login with Google
 |--------------------------------------------------------------------------
 */
 var express = require('express');
 var bodyParser = require('body-parser');
var router = express.Router();
var app = require('../../app');
var config = require('../../config/config');
var request = require('request');
var mongoose = require('mongoose');
var createToken = require('../../utils/utils');
var jwt = require('jwt-simple');
var BLOX = require('../../config/url_config');
var login = require('../../utils/utils');
//console.log(utils);


//console.log(app);

var mongoose = require('mongoose')
  , User = mongoose.model('userModel');


app.post('/auth/google', function(req, res) {

  var accessTokenUrl = 'https://accounts.google.com/o/oauth2/token';
  var peopleApiUrl = 'https://www.googleapis.com/plus/v1/people/me/openIdConnect';
  var params = {
    code: req.body.code,
    client_id: req.body.clientId,
    client_secret: config.GOOGLE_SECRET,
    redirect_uri: req.body.redirectUri,
    grant_type: 'authorization_code'
  };
console.log('googleAuth');
  
  //Step 1. Exchange authorization code for access token.
  request.post(accessTokenUrl, { json: true, form: params }, function(err, response, token) {
    var accessToken = token.access_token;
    var headers = { Authorization: 'Bearer ' + accessToken };

    // Step 2. Retrieve profile information about the current user.
     request.get({ url: peopleApiUrl, headers: headers, json: true }, function(err, response, profile) {

      console.log('google'+profile);

      //profile object has the profile.
	
      // Step 3a. Link user accounts.
      login.getRequestToken(profile.sub, null, 'google', function(req_token) {
        login.getAccessToken(req_token, function(access_token) {
	      res.send({token: access_token});
        });
      });
    });
  });
});

module.exports = router;
