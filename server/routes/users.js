
var express = require('express');
var bodyParser = require('body-parser');
var router = express.Router();
var app = require('../app');
var config = require('../config/config');
var request = require('request');
var qs = require('querystring');
var createToken = require('../utils/utils');
var jwt = require('jwt-simple');
var session = require('session');


var mongoose = require('mongoose')
    , Medication = mongoose.model('medicationModel'),
Test = mongoose.model('testResultModel')
    ;

/* GET users listing. */
app.get('/insertMed', function(req, res, next) {
    var testVal= {
        _id:mongoose.Types.ObjectId(),
        email:'test2@test.com',
        "date_time": {

        "low": {
            "date": "2008-01-03T00:00:00Z",
            "precision": "day"
        },
        "high": {
            "date": "2008-01-21T00:00:00Z",
            "precision": "day"
        }
    },
        "identifiers": [{
            "identifier": "cdbd33f0-6cde-11db-9fe1-0800200c9a66"
        }],
        "sig": "Proventil HFA",
        "product": {
            "identifiers": [{
                "identifier": "2a620155-9d11-439e-92b3-5d9815ff4ee8"
            }],
            "unencoded_name": "Proventil HFA",
            "product": {
                "name": "Proventil HFA",
                "code": "219483",
                "translations": [{
                    "name": "Proventil 0.09 MG/ACTUAT inhalant solution",
                    "code": "573621",
                    "code_system_name": "RXNORM"
                }],
                "code_system_name": "RXNORM"
            },
            "manufacturer": "Medication Factory Inc."
        },
        "supply": {
            "date_time": {
                "low": {
                    "date": "2007-01-03T00:00:00Z",
                    "precision": "day"
                }
            },
            "repeatNumber": "1",
            "quantity": "75",
            "author": {
                "identifiers": [{
                    "identifier": "2a620155-9d11-439e-92b3-5d9815fe4de8"
                }],
                "name": {
                    "prefix": "Dr.",
                    "last": "Seven",
                    "first": "Henry"
                }
            }
        },
        "administration": {
            "route": {
                "name": "RESPIRATORY (INHALATION)",
                "code": "C38216",
                "code_system_name": "Medication Route FDA"
            },
            "dose": {
                "value": 1,
                "unit": "mg/actuat"
            },
            "form1": {
                "name": "SOLUTION",
                "code": "C42986",
                "code_system_name": "Medication Route FDA"
            },
            "form": {
                "name": "",
                "code": "",
                "code_system_name": ""
            },
            "rate": {
                "value": 90,
                "unit": "ml/min"
            },
            "interval": {
                "period": {
                    "value": 6,
                    "unit": "h"
                },
                "frequency": true
            }
        },
        "performer": {
            "organization": [{
                "identifiers": [{
                    "identifier": "2.16.840.1.113883.19.5.9999.1393"
                }],
                "name": [
                    "Community Health and Hospitals"
                ]
            }]
        },
        "drug_vehicle": {
            "name": "Aerosol",
            "code": "324049",
            "code_system_name": "RXNORM"
        },
        "precondition": {
            "code": {
                "code": "ASSERTION",
                "code_system_name": "HL7ActCode"
            },
            "value": {
                "name": "Wheezing",
                "code": "56018004",
                "code_system_name": "SNOMED CT"
            }
        },
        "indication": {
            "identifiers": [{
                "identifier": "db734647-fc99-424c-a864-7e3cda82e703",
                "extension": "45665"
            }],
            "code": {
                "name": "Finding",
                "code": "404684003",
                "code_system_name": "SNOMED CT"
            },
            "date_time": {
                "low": {
                    "date": "2007-01-03T00:00:00Z",
                    "precision": "day"
                }
            },
            "value": {
                "name": "Pneumonia",
                "code": "233604007",
                "code_system_name": "SNOMED CT"
            }
        },
        "dispense": {
            "identifiers": [{
                "identifier": "1.2.3.4.56789.1",
                "extension": "cb734647-fc99-424c-a864-7e3cda82e704"
            }],
            "performer": {
                "identifiers": [{
                    "identifier": "2.16.840.1.113883.19.5.9999.456",
                    "extension": "2981823"
                }],
                "address": [{
                    "street_lines": [
                        "1001 Village Avenue"
                    ],
                    "city": "Portland",
                    "state": "OR",
                    "zip": "99123",
                    "country": "US"
                }],
                "organization": [{
                    "identifiers": [{
                        "identifier": "2.16.840.1.113883.19.5.9999.1393"
                    }],
                    "name": [
                        "Community Health and Hospitals"
                    ]
                }]
            }
        }

    }
    console.log('saving');
    var medication = new Medication(testVal);
    medication.save(function (err) {
        if (err) // ...
            console.log(err);
        res.send(err)
    });
    console.log('saved');
    //
});

app.get('/insertTestResults', function(req, res, next) {
    var testVal= {
        _id:mongoose.Types.ObjectId(),
        email:'mohit@privasera.com',

        "identifiers": [{
            "identifier": "7d5a02b0-67a4-11db-bd13-0800200c9a66"
        }],
        "result_set": {
            "name": "CBC WO DIFFERENTIAL",
            "code": "43789009",
            "code_system_name": "SNOMED CT"
        },
        "results": [{
            "identifiers": [{
                "identifier": "107c2dc0-67a5-11db-bd13-0800200c9a66"
            }],
            "result": {
                "name": "HGB",
                "code": "30313-1",
                "code_system_name": "LOINC"
            },
            "date_time": {
                "point": {
                    "date": "2000-03-23T14:30:00Z",
                    "precision": "minute"
                }
            },
            "status": "completed",
            "reference_range": {
                "range": "M 13-18 g/dl; F 12-16 g/dl"
            },
            "interpretations": [
                "Normal"
            ],
            "value": 13.2,
            "unit": "g/dl"
        }, {
            "identifiers": [{
                "identifier": "107c2dc0-67a5-11db-bd13-0800200c9a66"
            }],
            "result": {
                "name": "WBC",
                "code": "33765-9",
                "code_system_name": "LOINC"
            },
            "date_time": {
                "point": {
                    "date": "2000-03-23T14:30:00Z",
                    "precision": "minute"
                }
            },
            "status": "completed",
            "reference_range": {
                "low": "4.3",
                "high": "10.8",
                "unit": "10+3/ul"
            },
            "interpretations": [
                "Normal"
            ],
            "value": 6.7,
            "unit": "10+3/ul"
        }, {
            "identifiers": [{
                "identifier": "107c2dc0-67a5-11db-bd13-0800200c9a66"
            }],
            "result": {
                "name": "PLT",
                "code": "26515-7",
                "code_system_name": "LOINC"
            },
            "date_time": {
                "point": {
                    "date": "2000-03-23T14:30:00Z",
                    "precision": "minute"
                }
            },
            "status": "completed",
            "reference_range": {
                "low": "150",
                "high": "350",
                "unit": "10+3/ul"
            },
            "interpretations": [
                "Low"
            ],
            "value": 123,
            "unit": "10+3/ul"
        }]

    }
    console.log('saving test Results');
    var test = new Test(testVal);
    test.save(function (err) {
        if (err) // ...
            console.log(err);
        res.send(err)
    });
    console.log('saved');
    //
});


app.post('/login', function(req, res, next) {

    users = ['casen@privasera.com', 'mohit@privasera.com'];
    var valid_password = 'pass';

    var email = req.body.email;
    var password = req.body.password;

    if(users.indexOf(email) > -1 && password == valid_password) {
        session.email = req.body.email;
        res.send({'Result': 'Success'});
    }
    else {
        res.send({'Result': 'Failure'});
    } 
});


app.get('/user', function(req, res, next) {
    var success = true;
    if(session.email == 'casen@privasera.com') {
        var name = 'Casen Hunger';
        var email = session.email;
        var dob = '12/30/1991';
        var img = '/images/casen.jpg';
    }
    else if(session.email == 'mohit@privasera.com') {
        var name = 'Mohit Tiwari';
        var email = session.email;
        var dob = '2/23/1984';
        var img = '/images/mohit.jpg';
    }
    else {
        success = false;
    }
   
    if(success) {
        res.send({
            'Result': 'Success',
            'Name': name,
            'Email': email,
            'DOB': dob,
            'Image': img
        });
    }
    else {
        res.send({
            'Result': 'Failure'
        });
    }
});

module.exports = router;
