
var express = require('express');
var bodyParser = require('body-parser');
var router = express.Router();
var app = require('../../app');
var config = require('../../config/config');
var request = require('request');
var mongoose = require('mongoose');
var createToken = require('../../utils/utils');
var jwt = require('jwt-simple');
var multiparty = require('multiparty');
var util = require('util');

var BLOX = require('../../config/url_config');
var http = require("http");
var FormData = require('form-data');
var fs = require('fs');

var mongoose = require('mongoose')
    , User = mongoose.model('userModel');

console.log('starting application apis')
///*
// |--------------------------------------------------------------------------
// | GET /api/me
// |--------------------------------------------------------------------------
// */
//app.get('/api/me', ensureAuthenticated, function(req, res) {
//  User.findById(req.user, function(err, user) {
//    res.send(user);
//  });
//});
//
///*
// |--------------------------------------------------------------------------
// | PUT /api/me
// |--------------------------------------------------------------------------
// */
//app.put('/api/me', ensureAuthenticated, function(req, res) {
//  User.findById(req.user, function(err, user) {
//    if (!user) {
//      return res.status(400).send({ message: 'User not found' });
//    }
//    user.displayName = req.body.displayName || user.displayName;
//    user.email = req.body.email || user.email;
//    user.save(function(err) {
//      res.status(200).end();
//    });
//  });
//});


/*
 |--------------------------------------------------------------------------
 | GET /api/blox
 |--------------------------------------------------------------------------
 */

app.get('/api/blox',function(req, res) {
    var token = req.headers.authorization.split(' ')[1];
//    var loginId =  req.headers.loginId;
//    console.log('login'+loginId);
    /*
     {form: { access_token: token}}
     */
    var params = {
        access_token:token

    }
    request.get(BLOX.URL.BUBBLES, {json:params}, function(err, response, result) {
        if(err)
            throw err;
        console.log('get bubbles')
        console.log(result);
        //var result = JSON.parse(result);
        console.log(result);
        res.setHeader('Content-Type', 'application/json');
        res.send(result.bubbles);

    });
});

/*
 |--------------------------------------------------------------------------
 | POST /api/blox
 |--------------------------------------------------------------------------
 */
app.post('/api/blox',function(req, res) {
    var token = req.headers.authorization.split(' ')[1];
    var description = req.body.description;
    var name = req.body.name;
    var id =  req.body.id;
    /*
     {form: { access_token: token}}
     */
    var params = {
        access_token:token,
        name:name,
        description:description,
        id:id

    }
    console.log('firing post on bubbles')

    if(id!=""&&id!=undefined)
    {
        request.put(BLOX.URL.BUBBLES, {json:params}, function(err, response, result) {
            if(err)
                throw err;
            console.log('put bubbles')
            console.log(result);
//        var result = JSON.parse(result);
//        console.log(result);
//        res.setHeader('Content-Type', 'application/json');
        res.send(result);

        });

    }
    else
        request.post(BLOX.URL.BUBBLES, {json:params}, function(err, response, result) {
            if(err)
                throw err;
            console.log('post bubbles')
            console.log(result);
            //        var result = JSON.parse(result);
            //        console.log(result);
            //        res.setHeader('Content-Type', 'application/json');
                    res.send(result.bubble);

        });
});


/*
 |--------------------------------------------------------------------------
 | GET /api/application
 |--------------------------------------------------------------------------
 */

app.get('/api/applications',function(req, res) {
    var token = req.headers.authorization.split(' ')[1];
    /*
     {form: { access_token: token}}
     */
    var params = {
        access_token:token

    }
    request.get(BLOX.URL.APPS,params , function(err, response, result) {
        if(err)
            throw err;
        console.log('get applications')
        console.log(result);
        var result = JSON.parse(result);
        console.log(result);
        res.setHeader('Content-Type', 'application/json');
        res.send(result.apps);

    });
});

/*
 |--------------------------------------------------------------------------
 | POST /api/applications
 |--------------------------------------------------------------------------
 */
app.post('/api/applications',function(req, res) {
    var token = req.headers.authorization.split(' ')[1];
    var description = req.body.description;
    var name = req.body.name;
    var id =  req.body.id;
    /*
     {form: { access_token: token}}
     */
    var params = {
        access_token:token,
        name:name,
        description:description,
        id:id,
        app:'base64'

    }

    console.log('json:'+params)

    if(id!=""&&id!=undefined)
    {
        request.put(BLOX.URL.APPS, {json:params}, function(err, response, result) {
            if(err)
                throw err;
            console.log('put apis')
            console.log(result);
//        var result = JSON.parse(result);
//        console.log(result);
//        res.setHeader('Content-Type', 'application/json');
//        res.send(result.bubbles);

        });

    }
    else
        request.post(BLOX.URL.APPS, {json:params}, function(err, response, result) {
            if(err)
                throw err;
            console.log('post apis')
            console.log(result.bubble);
            //        var result = JSON.parse(result);
            //        console.log(result);
            //        res.setHeader('Content-Type', 'application/json');
                    res.send(result.bubbles);

        });
});

app.get('/api/blox/update',function(req, res) {

    var token = req.headers.authorization.split(' ')[1];

    var userName = req.headers.username;
        var params = {
         access_token:token,
        username:userName
    }
    console.log('update blox for '+userName);
    request.get(BLOX.URL.BUBBLES_UPDATE, {json:params}, function(err, response, result) {
        if(err)
            throw err;
        console.log('updated blox')
        console.log(result);
        //        var result = JSON.parse(result);
        //        console.log(result);
        //        res.setHeader('Content-Type', 'application/json');
        res.send(result.updates);

    });

})

app.post('/api/blox/upload',function(req, res) {

    var token = req.headers.authorization.split(' ')[1];
    var count = 0;
    var file = "";
    var fileName=""
    var options={
        autoFiles:false
    };

    var form = new multiparty.Form(options);
    var pForm = new FormData();
    var bubbleId=null;

    var destPath;
    var contentType;
    form.on('field', function(name, value) {
        if (name === 'path') {
            destPath = value;
        }
        else if (name==='id')
        {
            bubbleId=value;
        }

    });

//    form.parse(req, function(err, fields, files) {
//        res.writeHead(200, {'content-type': 'text/plain'});
//        res.write('received upload:\n\n');
//        res.end(util.inspect({fields: fields, files: files}));
////    });
//    form.on('part', function(part) {
//        // You *must* act on the part by reading it
//        // NOTE: if you want to ignore it, just call "part.resume()"
//
//
//
//        if (part.filename === null) {
//            // filename is "null" when this is a field and not a file
//            console.log('got field named ' + part.name);
//            // ignore field's content
//            part.resume();
//        }
//
//        if (part.filename !== null) {
//            // filename is not "null" when this is a file
//            count++;
//            console.log('got file named ' + part.fileName);
//            fileName=part.filename;
//            contentType=part.headers["content-type"];
//            file=part;
//            // ignore file's content here
//           // part.resume();
//            var formData = {
//                id:bubbleId,
//                fileData:file,
//                custom_file: {
//                    value:  file,
//                    options: {
//                        filename: fileName,
//                        contentType: contentType
//                    }
//                }
//            }
//            request.post(BLOX.URL.BUBBLES_FILES, {formData:formData}, function(err, response, result) {
//                if(err)
//                    throw err;
//                console.log('updated blox')
//                console.log(result);
//                var result = JSON.parse(result);
//                //        console.log(result);
//                //        res.setHeader('Content-Type', 'application/json');
//                res.send(result.result);
//
//            });
//
//
//        }
//
//        part.on('error', function(err) {
//            // decide what to do
//        });
//        part.on('data', function(buffer){
//            //file += buffer;
//        });
//    });
//
//// Close emitted after form parsed
////    form.on('close', function() {
////        console.log('Upload completed!');
////        res.setHeader('header', {'content-type': 'text/plain'});
////        res.end('Received ' + count + ' files');
////    });
//    form.on('close', function()
//        {
//
////            var params ={
////                id:"",
////                file:file
////            }
////            pForm.append()
//
//
////            res.writeHead(200, {
////                "Content-Type": "text/plain"
////            });
////            res.write(file);
////            res.end();
//
//
//
//
//
//        });
//
//// Parse req
//    form.parse(req);
    form.parse(req, function(err, fields, files) {
        Object.keys(fields).forEach(function(name) {
            console.log('got field named ' + name);
        });

        Object.keys(files).forEach(function(name) {
            console.log('got file named ' + name);
        });

        for(var i=0;i<files.file.length;i++)
        {
            var file = files.file[i];
            contentType=file.headers["content-type"];

            var formData = {
            id:bubbleId,
            access_token:token,
//            file:fs.createReadStream(file.path)
            fileName:file.originalFilename,
            custom_file: {
                value:  fs.createReadStream(file.path),
                options: {
                    filename: file.originalFilename,
                    contentType: contentType
                }
            }
        }
            request.post(BLOX.URL.BUBBLES_FILES, {formData:formData}, function(err, response, result) {
                if(err)
                    throw err;
                console.log('updated blox');
                console.log(result);
                        var result = JSON.parse(result);
                //        console.log(result);
                //        res.setHeader('Content-Type', 'application/json');
                res.send(result.result);

            });

        }

        console.log('Upload completed!');

    });

    console.log('FILE UPLOAD ');


})

/*
 |--------------------------------------------------------------------------
 | GET /api/blox/files
 |--------------------------------------------------------------------------
 */

app.get('/api/blox/files',function(req, res) {
    var token = req.headers.authorization.split(' ')[1];
    var bubbleId =  req.headers.id;
//    console.log('login'+loginId);
    /*
     {form: { access_token: token}}
     */

    console.log('get files for bubble')
    var params = {
        access_token:token,
        id:bubbleId

    }
    request.get(BLOX.URL.BUBBLES_FILES,{json:params} , function(err, response, result) {
        if(err)
            throw err;
       // console.log('get bubbles')
        console.log(result);
        //var result = JSON.parse(result);
        //console.log(result);
        //res.setHeader('Content-Type', 'application/json');
        res.send(result);

    });
});

/*
 |--------------------------------------------------------------------------
 | Notifications
 | GET /api/blox/notifications
 |--------------------------------------------------------------------------
 */

app.get('/api/blox/notifications',function(req, res) {
    var token = req.headers.authorization.split(' ')[1];
    var type = req.headers.type;

    var params = {
        access_token:token,
        type:type


    }
    request.get(BLOX.URL.BUBBLES_NOTIFICATIONS,{json:params} , function(err, response, result) {
        if(err)
            throw err;
        // console.log('get bubbles')
        console.log(result);
        //var result = JSON.parse(result);
        //console.log(result);
        //res.setHeader('Content-Type', 'application/json');
        res.send(result);

    });
});
module.exports = router;