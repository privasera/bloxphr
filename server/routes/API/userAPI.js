
var express = require('express');
 var bodyParser = require('body-parser');
var router = express.Router();
var app = require('../../app');
var config = require('../../config/config');
var request = require('request');
var mongoose = require('mongoose');
var createToken = require('../../utils/utils');
var jwt = require('jwt-simple');

var BLOX = require('../../config/url_config');

var mongoose = require('mongoose')
  , User = mongoose.model('userModel');

console.log('starting user apis')

/*
 |--------------------------------------------------------------------------
 | GET /api/blox
 |--------------------------------------------------------------------------
 */

app.post('/api/user/autocomplete',function(req, res) {
    var token = req.headers.authorization.split(' ')[1];
    var description = req.body.value;
    console.log('autocomplete')
    console.log(description);
    var params = {
        access_token:token,
        text:description
    }

    request.post(BLOX.URL.USER_AUTOCOMPLETE, {json:params}, function(err, response, result) {
        if(err)
            throw err;
        console.log('post apis')
        console.log(result);
        //        var result = JSON.parse(result);
        //        console.log(result);
        //        res.setHeader('Content-Type', 'application/json');
              res.send(result.names);

    });

});

/*
 |--------------------------------------------------------------------------
 | GET /api/user/share - Sharing of bubbles with users
 |--------------------------------------------------------------------------
 */
app.put('/api/user/share',function(req, res) {
    console.log(req.body.blox);
    var token = req.headers.authorization.split(' ')[1];
    var users = req.body.blox.users;
    var bubbles = req.body.blox.bubbles;
    var permission = req.body.blox.permission;
    var message = req.body.blox.message;
      var sharingBlock = req.body.blox;
    console.log('sharing');

    params = {
        access_token:token,
        users:users,
        bubbles:bubbles,
        permission:permission,
        message:message
    }
    console.log(params);
    request.put(BLOX.URL.BUBBLES_SHARE, {json:params}, function(err, response, result) {
        if(err)
            throw err;
        console.log('share apis')
        console.log(result);
        //        var result = JSON.parse(result);
        //        console.log(result);
        //        res.setHeader('Content-Type', 'application/json');
        res.send(result.names);

    });




});



module.exports = router;