
var express = require('express');
var bodyParser = require('body-parser');
var router = express.Router();
var app = require('../../../app');
var config = require('../../../config/config');
var request = require('request');
var qs = require('querystring');
var createToken = require('../../../utils/utils');
var jwt = require('jwt-simple');
var url = require('url');
var session = require('session');


var mongoose = require('mongoose')
    , Medication = mongoose.model('medicationModel')
    ,TestResult = mongoose.model('testResultModel')
    ;

/* GET users listing. */
app.get('/getTestResults', function(req, res, next) {
    //fetch the medications based on user id
    var email = req.headers.email;
    var args = url.parse(req.url, true).query;
    email = args.email.split(';');
    check_email = email[0];
    second_email = email[1];
    console.log("Email");
    console.log(email);
    console.log(session.email);

    //if(check_email != session.email) {
    //if(false) {
    //    res.send("Invalid Email");
    //}
    //else {

    //    if(email==undefined || email ==null||email=="")
    //    {
    //        email=req.body.email;
    //        if(email==undefined || email ==null||email=="")
    //            email='test@test.com';

    //    }
    //    console.log('fetching medications');
    //    var testRes = TestResult.find({email: {$in: email}},function(err,docs){
    //        if (err)
    //            res.send(err);
    //        console.log('error is '+err);

    //        console.log(docs);
    //        res.send(docs);
    //    });
    //}

    var testValCasen = {
        _id:mongoose.Types.ObjectId(),
        email:'casen@privasera.com',
        "date_time": {

        "low": {
            "date": "2008-01-03T00:00:00Z",
            "precision": "day"
        },
        "high": {
            "date": "2008-01-21T00:00:00Z",
            "precision": "day"
        }
    },
        "identifiers": [{
            "identifier": "cdbd33f0-6cde-11db-9fe1-0800200c9a66"
        }],
        "sig": "Proventil HFA",
        "product": {
            "identifiers": [{
                "identifier": "2a620155-9d11-439e-92b3-5d9815ff4ee8"
            }],
            "unencoded_name": "Proventil HFA",
            "product": {
                "name": "Proventil HFA",
                "code": "219483",
                "translations": [{
                    "name": "Proventil 0.09 MG/ACTUAT inhalant solution",
                    "code": "573621",
                    "code_system_name": "RXNORM"
                }],
                "code_system_name": "RXNORM"
            },
            "manufacturer": "Medication Factory Inc."
        },
        "supply": {
            "date_time": {
                "low": {
                    "date": "2007-01-03T00:00:00Z",
                    "precision": "day"
                }
            },
            "repeatNumber": "1",
            "quantity": "75",
            "author": {
                "identifiers": [{
                    "identifier": "2a620155-9d11-439e-92b3-5d9815fe4de8"
                }],
                "name": {
                    "prefix": "Dr.",
                    "last": "Seven",
                    "first": "Henry"
                }
            }
        },
        "administration": {
            "route": {
                "name": "RESPIRATORY (INHALATION)",
                "code": "C38216",
                "code_system_name": "Medication Route FDA"
            },
            "dose": {
                "value": 1,
                "unit": "mg/actuat"
            },
            "form1": {
                "name": "SOLUTION",
                "code": "C42986",
                "code_system_name": "Medication Route FDA"
            },
            "form": {
                "name": "",
                "code": "",
                "code_system_name": ""
            },
            "rate": {
                "value": 90,
                "unit": "ml/min"
            },
            "interval": {
                "period": {
                    "value": 6,
                    "unit": "h"
                },
                "frequency": true
            }
        },
        "performer": {
            "organization": [{
                "identifiers": [{
                    "identifier": "2.16.840.1.113883.19.5.9999.1393"
                }],
                "name": [
                    "Community Health and Hospitals"
                ]
            }]
        },
        "drug_vehicle": {
            "name": "Aerosol",
            "code": "324049",
            "code_system_name": "RXNORM"
        },
        "precondition": {
            "code": {
                "code": "ASSERTION",
                "code_system_name": "HL7ActCode"
            },
            "value": {
                "name": "Wheezing",
                "code": "56018004",
                "code_system_name": "SNOMED CT"
            }
        },
        "indication": {
            "identifiers": [{
                "identifier": "db734647-fc99-424c-a864-7e3cda82e703",
                "extension": "45665"
            }],
            "code": {
                "name": "Finding",
                "code": "404684003",
                "code_system_name": "SNOMED CT"
            },
            "date_time": {
                "low": {
                    "date": "2007-01-03T00:00:00Z",
                    "precision": "day"
                }
            },
            "value": {
                "name": "Pneumonia",
                "code": "233604007",
                "code_system_name": "SNOMED CT"
            }
        },
        "dispense": {
            "identifiers": [{
                "identifier": "1.2.3.4.56789.1",
                "extension": "cb734647-fc99-424c-a864-7e3cda82e704"
            }],
            "performer": {
                "identifiers": [{
                    "identifier": "2.16.840.1.113883.19.5.9999.456",
                    "extension": "2981823"
                }],
                "address": [{
                    "street_lines": [
                        "1001 Village Avenue"
                    ],
                    "city": "Portland",
                    "state": "OR",
                    "zip": "99123",
                    "country": "US"
                }],
                "organization": [{
                    "identifiers": [{
                        "identifier": "2.16.840.1.113883.19.5.9999.1393"
                    }],
                    "name": [
                        "Community Health and Hospitals"
                    ]
                }]
            }
        }
    }

    var testValMohit = {
        _id:mongoose.Types.ObjectId(),
        email:'mohit@privasera.com',
        "date_time": {

        "low": {
            "date": "2008-01-03T00:00:00Z",
            "precision": "day"
        },
        "high": {
            "date": "2008-01-21T00:00:00Z",
            "precision": "day"
        }
    },
        "identifiers": [{
            "identifier": "cdbd33f0-6cde-11db-9fe1-0800200c9a66"
        }],
        "sig": "Proventil HFA",
        "product": {
            "identifiers": [{
                "identifier": "2a620155-9d11-439e-92b3-5d9815ff4ee8"
            }],
            "unencoded_name": "Proventil HFA",
            "product": {
                "name": "Proventil HFA",
                "code": "219483",
                "translations": [{
                    "name": "Proventil 0.09 MG/ACTUAT inhalant solution",
                    "code": "573621",
                    "code_system_name": "RXNORM"
                }],
                "code_system_name": "RXNORM"
            },
            "manufacturer": "Medication Factory Inc."
        },
        "supply": {
            "date_time": {
                "low": {
                    "date": "2007-01-03T00:00:00Z",
                    "precision": "day"
                }
            },
            "repeatNumber": "1",
            "quantity": "75",
            "author": {
                "identifiers": [{
                    "identifier": "2a620155-9d11-439e-92b3-5d9815fe4de8"
                }],
                "name": {
                    "prefix": "Dr.",
                    "last": "Seven",
                    "first": "Henry"
                }
            }
        },
        "administration": {
            "route": {
                "name": "RESPIRATORY (INHALATION)",
                "code": "C38216",
                "code_system_name": "Medication Route FDA"
            },
            "dose": {
                "value": 1,
                "unit": "mg/actuat"
            },
            "form1": {
                "name": "SOLUTION",
                "code": "C42986",
                "code_system_name": "Medication Route FDA"
            },
            "form": {
                "name": "",
                "code": "",
                "code_system_name": ""
            },
            "rate": {
                "value": 90,
                "unit": "ml/min"
            },
            "interval": {
                "period": {
                    "value": 6,
                    "unit": "h"
                },
                "frequency": true
            }
        },
        "performer": {
            "organization": [{
                "identifiers": [{
                    "identifier": "2.16.840.1.113883.19.5.9999.1393"
                }],
                "name": [
                    "Community Health and Hospitals"
                ]
            }]
        },
        "drug_vehicle": {
            "name": "Aerosol",
            "code": "324049",
            "code_system_name": "RXNORM"
        },
        "precondition": {
            "code": {
                "code": "ASSERTION",
                "code_system_name": "HL7ActCode"
            },
            "value": {
                "name": "Wheezing",
                "code": "56018004",
                "code_system_name": "SNOMED CT"
            }
        },
        "indication": {
            "identifiers": [{
                "identifier": "db734647-fc99-424c-a864-7e3cda82e703",
                "extension": "45665"
            }],
            "code": {
                "name": "Finding",
                "code": "404684003",
                "code_system_name": "SNOMED CT"
            },
            "date_time": {
                "low": {
                    "date": "2007-01-03T00:00:00Z",
                    "precision": "day"
                }
            },
            "value": {
                "name": "Pneumonia",
                "code": "233604007",
                "code_system_name": "SNOMED CT"
            }
        },
        "dispense": {
            "identifiers": [{
                "identifier": "1.2.3.4.56789.1",
                "extension": "cb734647-fc99-424c-a864-7e3cda82e704"
            }],
            "performer": {
                "identifiers": [{
                    "identifier": "2.16.840.1.113883.19.5.9999.456",
                    "extension": "2981823"
                }],
                "address": [{
                    "street_lines": [
                        "1001 Village Avenue"
                    ],
                    "city": "Portland",
                    "state": "OR",
                    "zip": "99123",
                    "country": "US"
                }],
                "organization": [{
                    "identifiers": [{
                        "identifier": "2.16.840.1.113883.19.5.9999.1393"
                    }],
                    "name": [
                        "Community Health and Hospitals"
                    ]
                }]
            }
        }
    }

    if(check_email != session.email) {
        res.send("Invalid email access");
    }
    else if(second_email != undefined) {
        res.send([testValCasen, testValMohit]);
    }
    else if(check_email == "casen@privasera.com") {
        res.send(testValCasen);
    }
    else if(check_email == "mohit@privasera.com") {
        res.send(testValMohit);
    }
    else {
        res.send("Records not found");
    }
    
});

module.exports = router;
