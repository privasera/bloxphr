var express = require('express');
var path = require('path');
var qs = require('querystring');
var router = express.Router();
var async = require('async');
var bcrypt = require('bcryptjs');
var bodyParser = require('body-parser');
var express = require('express');
var logger = require('morgan');
var jwt = require('jwt-simple');
var moment = require('moment');
var mongoose = require('mongoose');
var request = require('request');
var fs = require('fs');
var multiparty = require('multiparty');
var util = require('util');

var env = process.env.NODE_ENV || 'development'

var config = require('./config/dbConfig')[env];




var connect = function () {
  var options = { server: { socketOptions: { keepAlive: 1 } } }
  console.log(config);
  mongoose.connect(config.db, options)
}
//connect();


mongoose.connection.on('error', function (err) {
  console.log(err)
})

// Reconnect when closed
mongoose.connection.on('disconnected', function () {
  //connect()
})


var models_path = __dirname + '/models'
fs.readdirSync(models_path).forEach(function (file) {
    console.log(file);
  if (~file.indexOf('.js')) require(models_path + '/' + file)
})

//var routes = require('./routes/index');


var app = module.exports = express();



app.use(express.static(path.join(__dirname, '../client/app/')));







// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
//app.use(cookieParser());


var googleAuth = require('./routes/oauth/googleAuth');
var facebookAuth = require('./routes/oauth/facebookAuth');
var loginAuth = require('./routes/oauth/loginAuth');
var userApi = require('./routes/API/userAPI');
var applicationAPI = require('./routes/API/applicationAPI');
var medicationsAPI = require('./routes/API/medications/medicationsAPI');
var testResultsAPI = require('./routes/API/testresults/testResultsAPI');
var users =require('./routes/users');

/*
 |--------------------------------------------------------------------------
 | Login Required Middleware
 |--------------------------------------------------------------------------
 */
function ensureAuthenticated(req, res, next) {
  if (!req.headers.authorization) {
    return res.status(401).send({ message: 'Please make sure your request has an Authorization header' });
  }
  var token = req.headers.authorization.split(' ')[1];
  var payload = jwt.decode(token, config.BLOX_SECRET);
  if (payload.exp <= moment().unix()) {
    return res.status(401).send({ message: 'Token has expired' });
  }
  req.user = payload.sub;
  next();
}

//console.log('google auth is '+googleAuth);
//routes
//app.use('/auth/login ', loginAuth);
//app.use('/test', users);

// catch 404 and forward to error handler
// app.use(function(req, res, next) {
//     var err = new Error('Not Found');
//     err.status = 404;
//     next(err);
// });

// error handlers

// development error handler
// will print stacktrace
// if (app.get('env') === 'development') {
//     app.use(function(err, req, res, next) {
//         res.status(err.status || 500);
//         res.render('error', {
//             message: err.message,
//             error: err
//         });
//     });
// }
if (app.get('env') === 'production') {
  app.use(function(req, res, next) {
    var protocol = req.get('x-forwarded-proto');
    protocol == 'https' ? next() : res.redirect('https://' + req.hostname + req.url);
  });
}



// production error handler
// no stacktraces leaked to user
// app.use(function(err, req, res, next) {
//     res.status(err.status || 500);
//     res.render('error', {
//         message: err.message,
//         error: {}
//     });
// });







/*
 |--------------------------------------------------------------------------
 | Generate JSON Web Token
 |--------------------------------------------------------------------------
 */

module.exports=app;


