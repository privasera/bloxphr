 var express = require('express');
 var bodyParser = require('body-parser');
var router = express.Router();
var app = require('../app');
var request = require('request');
var jwt = require('jwt-simple');
var moment = require('moment');
var config = require('../config/config');
var BLOX = require('../config/url_config');

var login = {};


login.createToken = function(user) {
  var payload = {
    sub: user._id,
    iat: moment().unix(),
    exp: moment().add(14, 'days').unix()
  };
  return jwt.encode(payload, config.TOKEN_SECRET);
}


login.registerUser = function(username, password, type, callback) {
  params = {
    username: username,
    password: password,
    client_id: config.BLOX_ID,
    client_secret: config.BLOX_SECRET
  }
  request.post(BLOX.URL.USER, {json: params}, function(err, response, result) {
    if(err)
      throw err;
    if(result.result == "success") {
       login.getRequestToken(username, password, type, callback);
    } 
  });
}


login.getRequestToken = function(username, password, type, callback) {
  params = {
    type: type,
    username: username,
    password: password
  }
  request.post(BLOX.URL.LOGIN, {form: params}, function(err, response, req_token) {
    if(err)
      throw err;
    if(response.statusCode == 500)
      login.registerUser(username, password, type, callback);

    var req_token = JSON.parse(req_token);
    if(req_token.success) {
      callback(req_token);
    }
    else if(req_token.error)
    {
        callback(req_token);
    }

    /* Register the user if login failed */
    else {
      login.registerUser(username, password, type, callback);
    }
  });
}


login.getAccessToken = function(req_token, callback) {
  params = {
    client_id: config.BLOX_ID,
    client_secret: config.BLOX_SECRET,
    grant_type: "client_credentials",
    user_id: req_token.success   
  }

  request.post(BLOX.URL.TOKEN, {form: params}, function(err, response, token) {
    if(err)
      throw err;
   
    token = JSON.parse(token)
    if(token.result == "success")
      callback(token.access_token);
    else
      callback(false);
  });
}

module.exports = login
