var BLOX = BLOX || {};

BLOX.URL = {

  LOGIN: 'http://ljtrust.org:8080/oauth/login',
  TOKEN: 'http://ljtrust.org:8080/oauth/token',
  USER: 'http://ljtrust.org:8080/api/admin/users',
  USER_REGISTER: 'http://ljtrust.org:8080/api/admin/users',
  BUBBLES : 'http://ljtrust.org:8080/api/admin/bubbles',
  APPS :'http://ljtrust.org:8080/api/admin/apps',
  BUBBLES_POST : 'http://ljtrust.org:8080/api/admin/apps',
  USER_AUTOCOMPLETE : 'http://ljtrust.org:8080/api/admin/users/autocomplete',
  BUBBLES_SHARE:'http://ljtrust.org:8080/api/admin/users/share',
  BUBBLES_UPDATE:'http://ljtrust.org:8080/api/admin/users/update',
  BUBBLES_FILES : 'http://ljtrust.org:8080/api/admin/files',
  BUBBLES_NOTIFICATIONS:'http://ljtrust.org:8080/api/admin/notifications'
}

module.exports = BLOX;
