var mongoose = require('mongoose')
    , config = require('../config/config');
Schema = mongoose.Schema

var bcrypt = require('bcryptjs');

var medicationSchema = new mongoose.Schema({

    email: { type: String, lowercase: true },
    _id: { type: String,unique:true },
    date_time: {
        low: {
            date: Date,
            precision: String
        },
        high: {
            date: Date,
            precision:String
        }
    },
    identifiers: [{
        identifier: String
    }],
    sig: String,
    product: {
        identifiers: [{
            identifier: String
        }],
        unencoded_name: String,
        product: {
            name: String,
            code: String,
            translations: [{
                name: String,
                code: String,
                code_system_name: String
            }],
            code_system_name: String
        },
        manufacturer:String
    },
    supply: {
        date_time: {
            low: {
                date: String,
                precision: String
            }
        },
        repeatNumber: String,
        quantity: String,
        author: {
            identifiers: [{
                identifier: String
            }],
            name: {
                prefix: String,
                last: String,
                first:String
            }
        }
    },
    administration: {
        route: {
            name: String,
            code: String,
            code_system_name: String
        },
        dose: {
            value: Number,
            unit:String
        },
        form1: {
            name: String,
            code: String,
            code_system_name: String
        },
        form: {
            name:String,
            code: String,
            code_system_name: String
        },
        rate: {
            value: Number,
            unit: String
        },
        interval: {
            period: {
                value: Number,
                unit: String
            },
            frequency: Boolean
        }
    },
    performer: {
        organization: [{
            identifiers: [{
                identifier: String
            }],
            name: [
                  String
            ]
        }]
    },
    drug_vehicle: {
        name: String,
        code: String,
        code_system_name: String
    },
    precondition: {
        code: {
            code: String,
            code_system_name: String
        },
        value: {
            name: String,
            code: String,
            code_system_name: String
        }
    },
    indication: {
        identifiers: [{
            identifier: String,
            extension: String
        }],
        code: {
            name: String,
            code: String,
            code_system_name: String
        },
        date_time: {
            low: {
                date: Date,
                precision: String
            }
        },
        value: {
            name: String,
            code: String,
            code_system_name: String
        }
    },
    dispense: {
        identifiers: [{
            identifier: String,
            extension: String
        }],
        performer: {
            identifiers: [{
                identifier: String,
                extension: String
            }],
            address: [{
                street_lines: [
                    String
                ],
                city: String,
                state: String,
                zip: String,
                country: String
            }],
            organization: [{
                identifiers: [{
                    identifier: String
                }],
                name: [
                    String
                ]
            }]
        }
    }
});

//userSchema.pre('save', function(next) {
//    var user = this;
//    if (!user.isModified('password')) {
//        return next();
//    }
//    bcrypt.genSalt(10, function(err, salt) {
//        bcrypt.hash(user.password, salt, function(err, hash) {
//            user.password = hash;
//            next();
//        });
//    });
//});

//userSchema.methods.comparePassword = function(password, done) {
//    bcrypt.compare(password, this.password, function(err, isMatch) {
//        done(err, isMatch);
//    });
//};
//console.log(medicationSchema);
mongoose.model('medicationModel', medicationSchema);
module.exports = medicationSchema;