var mongoose = require('mongoose')
    , config = require('../config/config');
Schema = mongoose.Schema

var bcrypt = require('bcryptjs');

var testResultSchema = new mongoose.Schema({
    email: { type: String,lowercase: true },
    _id: { type: String,unique:true },

    "identifiers": [{
        "identifier": String
    }],
    "result_set": {
        "name": String,
        "code": String,
        "code_system_name": String
    },
    "results": [{
        "identifiers": [{
            "identifier": String
        }],
        "result": {
            "name": String,
            "code": String,
            "code_system_name": String
        },
        "date_time": {
            "point": {
                "date": String,
                "precision": String
            }
        },
        "status": String,
        "reference_range": {
            "range": String
        },
        "interpretations": [

        ],
        "value": Number,
        "unit": String
    }, {
        "identifiers": [{
            "identifier": String
        }],
        "result": {
            "name": String,
            "code": String,
            "code_system_name": String
        },
        "date_time": {
            "point": {
                "date": String,
                "precision":String
            }
        },
        "status":String,
        "reference_range": {
            "low": String,
            "high": String,
            "unit": String
        },
        "interpretations": [

        ],
        "value": Number,
        "unit": String
    }, {
        "identifiers": [{
            "identifier": String
        }],
        "result": {
            "name": String,
            "code": String,
            "code_system_name": String
        },
        "date_time": {
            "point": {
                "date": String,
                "precision": String
            }
        },
        "status": String,
        "reference_range": {
            "low": String,
            "high": String,
            "unit": String
        },
        "interpretations": [

        ],
        "value": Number,
        "unit":String
    }]
});

//userSchema.pre('save', function(next) {
//    var user = this;
//    if (!user.isModified('password')) {
//        return next();
//    }
//    bcrypt.genSalt(10, function(err, salt) {
//        bcrypt.hash(user.password, salt, function(err, hash) {
//            user.password = hash;
//            next();
//        });
//    });
//});

//userSchema.methods.comparePassword = function(password, done) {
//    bcrypt.compare(password, this.password, function(err, isMatch) {
//        done(err, isMatch);
//    });
//};
//console.log(medicationSchema);
mongoose.model('testResultModel', testResultSchema);
//module.exports = medicationSchema;